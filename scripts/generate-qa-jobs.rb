#!/usr/bin/env ruby

class GenerateQAJobs
  def initialize(no_of_example_files)
    @no_of_example_files = no_of_example_files
    puts "no_of_example_files: #{@no_of_example_files}"
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/PerceivedComplexity
  # rubocop:disable Metrics/CyclomaticComplexity
  def execute
    jobs = load_yml_contents('base')
    jobs.concat(load_yml_contents('sanity_framework'))
    jobs.concat(load_yml_contents('custom_parallel'))
    jobs.concat(load_yml_contents('instance')) if should_run?('test_instance_all')
    jobs.concat(load_yml_contents('relative_url')) if should_run?('test_instance_all')
    jobs.concat(load_yml_contents('repository_storage')) if should_run?('test_instance_all_repository_storage')
    jobs.concat(load_yml_contents('omnibus_image'))
    jobs.concat(load_yml_contents('update')) if should_run?('test_instance_all')
    jobs.concat(load_yml_contents('omnibus_upgrade'))
    jobs.concat(load_yml_contents('ee_previous_to_ce_update'))
    jobs.concat(load_yml_contents('mattermost')) if should_run?('test_integration_mattermost')
    jobs.concat(load_yml_contents('service_ping_disabled')) if should_run?('test_integration_servicepingdisabled')
    jobs.concat(load_yml_contents('ldap_no_tls')) if should_run?('test_integration_ldapnotls')
    jobs.concat(load_yml_contents('ldap_tls')) if should_run?('test_integration_ldaptls')
    jobs.concat(load_yml_contents('ldap_no_server')) if should_run?('test_integration_ldapnoserver')
    jobs.concat(load_yml_contents('instance_saml')) if should_run?('test_integration_instancesaml')
    jobs.concat(load_yml_contents('group_saml')) if should_run?('test_integration_groupsaml')
    jobs.concat(load_yml_contents('object_storage')) if should_run?('test_instance_all_object_storage')
    jobs.concat(load_yml_contents('object_storage_aws')) if should_run?('test_instance_all_object_storage')
    jobs.concat(load_yml_contents('object_storage_gcs')) if should_run?('test_instance_all_object_storage')
    jobs.concat(load_yml_contents('object_storage_registry_tls')) if should_run?('test_integration_registrytls')
    jobs.concat(load_yml_contents('registry')) if should_run?('test_integration_registry')
    jobs.concat(load_yml_contents('packages')) if should_run?('test_instance_all_packages')
    jobs.concat(load_yml_contents('actioncable')) if should_run?('test_instance_all_actioncable')
    jobs.concat(load_yml_contents('elasticsearch')) if should_run?('test_integration_elasticsearch')
    jobs.concat(load_yml_contents('praefect')) if should_run?('test_instance_all')
    jobs.concat(load_yml_contents('gitaly_cluster')) if should_run?('test_instance_all')
    jobs.concat(load_yml_contents('mtls')) if should_run?('test_instance_all_mtls')
    jobs.concat(load_yml_contents('smtp')) if should_run?('test_integration_smtp')
    jobs.concat(load_yml_contents('jira')) if should_run?('test_instance_all_jira')
    jobs.concat(load_yml_contents('integrations')) if should_run?('test_instance_all_integrations')
    jobs.concat(load_yml_contents('large_setup')) if should_run?('test_instance_all_can_use_large_setup')
    jobs.concat(load_yml_contents('cloud_activation')) if should_run?('test_instance_all_cloud_activation')
    jobs.concat(load_yml_contents('registry_with_cdn')) if should_run?('test_integration_registrywithcdn')
    jobs.concat(load_yml_contents('staging'))

    # Disabling geo jobs temporarily due to https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/774
    # base.concat(load_yml_contents('geo')) if should_run?('scenario_test_geo')

    jobs
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/PerceivedComplexity
  # rubocop:enable Metrics/CyclomaticComplexity

  private

  def should_run?(example_file_name)
    @no_of_example_files.include?(example_file_name)
  end

  def load_yml_contents(file_prefix)
    jobs_dir_path = File.expand_path('../.gitlab/ci/jobs', __dir__)
    File.read(File.join(jobs_dir_path, "#{file_prefix}.gitlab-ci.yml"))
  end
end

jobs = GenerateQAJobs.new(Dir.glob('no_of_examples/*').map { |s| File.basename(s, '.*') }).execute

File.open('generated-qa-jobs.yml', 'w') { |f| f.write(jobs) }
