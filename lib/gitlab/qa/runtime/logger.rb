# frozen_string_literal: true

require 'forwardable'

module Gitlab
  module QA
    module Runtime
      class Logger
        extend SingleForwardable

        def_delegators :logger, :debug, :info, :warn, :error, :fatal, :unknown

        def self.logger
          @logger ||= QA::TestLogger.logger(level: Runtime::Env.debug? ? ::Logger::DEBUG : ::Logger::INFO)
        end
      end
    end
  end
end
