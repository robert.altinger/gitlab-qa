# frozen_string_literal: true

module Gitlab
  module QA
    module Docker
      class Engine
        DOCKER_HOST = ENV['DOCKER_HOST'] || 'http://localhost'
        PRIVILEGED_COMMANDS = [/^iptables.*/].freeze

        def hostname
          URI(DOCKER_HOST).host
        end

        def login(username:, password:, registry:)
          Docker::Command.execute(%(login --username "#{username}" --password "#{password}" #{registry}), mask_secrets: password)
        end

        def pull(image:, tag: nil)
          Docker::Command.execute("pull #{full_image_name(image, tag)}")
        end

        def run(image:, tag: nil, args: [])
          Docker::Command.new('run').tap do |command|
            yield command if block_given?

            command << full_image_name(image, tag)
            command << args if args.any?

            command.execute!
          end
        end

        def privileged_command?(command)
          PRIVILEGED_COMMANDS.each do |privileged_regex|
            return true if command.match(privileged_regex)
          end

          false
        end

        # Write to file(s) in the Docker container specified by @param name
        # @param name The name of the Docker Container
        # @example
        #   engine.write_files('gitlab-abc123') do |files|
        #     files.append('/etc/hosts', '127.0.0.1 localhost')
        #     files.write('/opt/other', <<~TEXT
        #       This is content
        #       That goes within /opt/other
        #     TEXT)
        def write_files(name)
          exec(name, yield(
            Class.new do
              def self.write(file, contents)
                %(echo "#{contents}" > #{file};)
              end

              def self.append(file, contents)
                %(echo "#{contents}" >> #{file};)
              end
            end
          ))
        end

        def exec(name, command)
          cmd = ['exec']
          cmd << '--privileged' if privileged_command?(command)
          Docker::Command.execute(%(#{cmd.join(' ')} #{name} bash -c "#{command.gsub('"', '\\"')}"))
        end

        def read_file(image, tag, path, &block)
          cat_file = "run --rm --entrypoint /bin/cat #{full_image_name(image, tag)} #{path}"
          Docker::Command.execute(cat_file, &block)
        end

        def attach(name, &block)
          Docker::Command.execute("attach --sig-proxy=false #{name}", &block)
        end

        def copy(name, src_path, dest_path)
          Docker::Command.execute("cp #{src_path} #{name}:#{dest_path}")
        end

        def restart(name)
          Docker::Command.execute("restart #{name}")
        end

        def stop(name)
          Docker::Command.execute("stop #{name}")
        end

        def remove(name)
          Docker::Command.execute("rm -f #{name}")
        end

        def container_exists?(name)
          Docker::Command.execute("container inspect #{name}")
        rescue Docker::Shellout::StatusError
          false
        else
          true
        end

        def network_exists?(name)
          Docker::Command.execute("network inspect #{name}")
        rescue Docker::Shellout::StatusError
          false
        else
          true
        end

        def network_create(name)
          Docker::Command.execute("network create #{name}")
        end

        def port(name, port)
          Docker::Command.execute("port #{name} #{port}/tcp")
        end

        def running?(name)
          Docker::Command.execute("ps -f name=#{name}").include?(name)
        end

        def ps(name = nil)
          Docker::Command.execute(['ps', name].compact.join(' '))
        end

        private

        def full_image_name(image, tag)
          [image, tag].compact.join(':')
        end
      end
    end
  end
end
