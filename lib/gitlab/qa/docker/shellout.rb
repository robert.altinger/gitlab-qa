require 'open3'
require 'rainbow/refinement'

module Gitlab
  module QA
    module Docker
      class Shellout
        using Rainbow
        StatusError = Class.new(StandardError)

        def initialize(command)
          @command = command
          @output = []

          Rainbow.enabled = Runtime::Env.colorized_logs?
          Runtime::Logger.info("Docker shell command: `#{@command.mask_secrets}`".cyan)
        end

        def execute! # rubocop:disable Metrics/AbcSize
          raise StatusError, 'Command already executed' if @output.any?

          Open3.popen2e(@command.to_s) do |_in, out, wait|
            out.each do |line|
              @output.push(line)

              if block_given?
                yield line, wait
              else
                puts line
              end
            end

            if wait.value.exited? && wait.value.exitstatus.nonzero? # rubocop:disable Style/IfUnlessModifier
              raise StatusError, "Docker command `#{@command.mask_secrets}` failed! ✘"
            end
          end

          @output.join.chomp
        end
      end
    end
  end
end
