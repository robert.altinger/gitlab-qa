module Gitlab
  module QA
    module Component
      class Release < Staging
        ADDRESS = 'https://release.gitlab.net'.freeze
      end
    end
  end
end
