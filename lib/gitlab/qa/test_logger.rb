# frozen_string_literal: true

require 'logger'
require 'rainbow'

module Gitlab
  module QA
    # Common test logger implementation
    #
    class TestLogger
      LEVEL_COLORS = {
        "DEBUG" => :magenta,
        "INFO" => :green,
        "WARN" => :yellow,
        "ERROR" => :red,
        "FATAL" => :indianred
      }.freeze

      Rainbow.enabled = Runtime::Env.colorized_logs?

      def self.logger(level: ::Logger::INFO, source: 'Gitlab QA')
        ::Logger.new($stdout).tap do |logger|
          logger.level = level

          logger.formatter = proc do |severity, datetime, progname, msg|
            date_format = datetime.strftime("%Y-%m-%d %H:%M:%S")
            msg_prefix = "[date=#{date_format} from=#{source}] #{severity.ljust(5)} -- "

            Rainbow(msg_prefix).public_send(LEVEL_COLORS.fetch(severity, :silver)) + "#{msg}\n" # rubocop:disable GitlabSecurity/PublicSend
          end
        end
      end
    end
  end
end
