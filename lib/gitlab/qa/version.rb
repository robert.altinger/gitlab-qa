# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '7.25.0'
  end
end
