module Gitlab
  module QA
    module Scenario
      module Test
        module Integration
          class Actioncable < Scenario::Template
            def perform(release, *rspec_args)
              Component::Gitlab.perform do |gitlab|
                gitlab.release = QA::Release.new(release)
                gitlab.name = 'gitlab-actioncable'
                gitlab.network = 'test'
                gitlab.omnibus_configuration << "actioncable['enable'] = true"

                gitlab.instance do
                  Runtime::Logger.info('Running actioncable specs!')

                  rspec_args << "--" unless rspec_args.include?('--')
                  rspec_args << %w[--tag actioncable]

                  Component::Specs.perform do |specs|
                    specs.suite = 'Test::Instance::All'
                    specs.release = gitlab.release
                    specs.network = gitlab.network
                    specs.args = [gitlab.address, *rspec_args]
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
