require 'json'
require 'net/http'
require 'cgi'
require 'time'

module Gitlab
  module QA
    module Scenario
      module Test
        module Sanity
          # This test checks that the sha_version of a GitLab was authored in
          # the window defined by the `weekday_hours` method.
          # We perform a single API call to get the commit
          class Version < Scenario::Template
            def perform(release = 'ce')
              version = Component::Gitlab.perform do |gitlab|
                gitlab.release = release
                gitlab.act do
                  pull
                  sha_version
                end
              end

              project = "gitlab-org/#{QA::Release.new(release).api_project_name}"
              commit = api_commit_detail(project, version)

              if commit_within_hours?(commit['created_at'], weekday_hours(commit['created_at']))
                puts "Found commit #{version} in recent history of #{project}"
              else
                puts "Did not find #{version} in recent history of #{project}"
                exit 1
              end
            end

            private

            def weekday_hours(date_string)
              case Date.parse(date_string).wday
                # Sunday
              when 0
                48
                # Monday
              when 1
                72
              else
                24
              end
            end

            def commit_within_hours?(commit_time_string, hours)
              Time.at(Time.parse(commit_time_string).utc).to_datetime > Time.at((Time.now - hours * 60 * 60).utc).to_datetime
            end

            def api_commit_detail(project, commit_id)
              api = 'https://gitlab.com/api/v4'
              url = "#{api}/projects/#{CGI.escape(project)}/repository/commits/#{commit_id}"

              JSON.parse(Net::HTTP.get(URI(url)))
            end
          end
        end
      end
    end
  end
end
